from urllib.parse import urlparse

from django.test import TestCase

from url_shorten.models import Link


class TestLink(TestCase):

    def test_create_link(self):
        """Can we new up a link?"""
        link_params = {'redirect_link': 'http://www.google.com/foo/bar/baz/'}
        link = Link.objects.create(**link_params)
        self.assertIsInstance(link, Link)

    def test_create_short_path(self):
        """If we pass in a vanity string do we get back our vanity link as the shortened link?"""
        vanity_path = 'myvanitypath'
        link_params = {'redirect_link': 'http://www.google.com/foo/bar/baz/', 'vanity': vanity_path}
        link = Link.objects.create(**link_params)

        result_parts = urlparse(link.short_url)
        self.assertEqual(result_parts[2], '/{}'.format(vanity_path))

    def test_multiple_short_paths(self):
        """Can we create multiple vanity links that point to the same url?"""
        vanity_path = 'myvanitypath'
        redirect_link = 'http://www.google.com/foo/bar/baz/'
        link_params = [{'redirect_link': redirect_link,
                        'vanity': '{}1'.format(vanity_path)},
                       {'redirect_link': redirect_link,
                        'vanity': '{}2'.format(vanity_path)},
                       {'redirect_link': redirect_link,
                        'vanity': '{}3'.format(vanity_path)}]
        for lp in link_params:
            Link.objects.create(**lp).save()
        links = Link.objects.all() # There should be 3
        self.assertEqual(links.count(), 3)

        for link in links:
            self.assertEqual(link.redirect_link, redirect_link)

    def test_redirect_link(self):
        """Do we get back the expected redirect url?"""
        expected = 'http://www.google.com/foo/bar/baz/'
        vanity_path = 'myvanitypath'
        link_params = {'redirect_link': expected, 'vanity': vanity_path}
        link = Link.objects.create(**link_params)
        actual = link.redirect_link
        self.assertEqual(actual, expected)

    def test_only_one_link(self):
        """If we try to shorten an identical link do we reuse the existing record?"""
        link_params = {'redirect_link': 'http://www.google.com/foo/bar/baz/'}
        link1 = Link.objects.create(**link_params)
        link2 = Link.objects.create(**link_params)
        self.assertEqual(1, Link.objects.all().count())
        self.assertEqual(link1.short_url, link2.short_url)
        self.assertEqual(link1.redirect_link, link2.redirect_link)