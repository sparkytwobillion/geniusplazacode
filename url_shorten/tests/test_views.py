import json

from django.test import TestCase, Client, RequestFactory

from url_shorten.models import Link, LinkView
from url_shorten.views import RedirectView, _last_thirty_days, StatsView


class TestRedirectView(TestCase):

    def setUp(self) -> None:
        self.link_params = {'redirect_link': 'http://www.google.com/foo/bar/baz/'}
        self.link = Link.objects.create(**self.link_params)
        self.factory = RequestFactory()

    def test_redirect_response(self):
        """Do we redirect when we receive a redirect url?"""

        view = RedirectView()
        request = self.factory.get('{}/'.format(self.link.short_url))
        response = view.get(request)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(self.link_params['redirect_link'], response.url)

    def test_incriments_views(self):
        """Do we increment the view if we redirect the link?"""
        LinkView.objects.all().delete()
        view = RedirectView()
        request = self.factory.get('{}/'.format(self.link.short_url))
        view.get(request)
        view.get(request)
        view.get(request)
        self.assertEqual(3, LinkView.objects.all().count())


class TestStatsView(TestCase):
    def setUp(self) -> None:
        self.link_params = {'redirect_link': 'http://www.google.com/foo/bar/baz/'}
        self.link = Link.objects.create(**self.link_params)
        self.client = Client()
        self.factory = RequestFactory()

    def tearDown(self) -> None:
        LinkView.objects.all().delete()

    def test_get_stats(self):
        """Can we get the stats for our given short url?"""
        request = self.factory.get('{}/'.format(self.link.short_url))
        view = RedirectView()
        view.get(request)
        view.get(request)
        view.get(request)
        self.assertEqual(3, LinkView.objects.all().count())
        data = json.dumps({'path': '{}/'.format(self.link.short_path)})
        stats_request = self.factory.post(path='/get-stats', data=data, content_type="application/json")
        stats_view = StatsView()
        result = stats_view.post(stats_request)
        result_dict = json.loads(result.content)
        self.assertEqual(result_dict['total_views'], 3)
        self.assertEqual(len(result_dict['histogram']), 31)
        self.assertEqual(result_dict['created'], self.link.date.strftime("%b %d %Y"))
