import json
from datetime import timedelta, datetime
from urllib.parse import urlparse

from django.http import JsonResponse, Http404
from django.shortcuts import redirect

from django.views import View

from url_shorten.models import Link, LinkView, VanityException


class CreateLink(View):

    def post(self, request):
        """Attempt to create a shortened link and return a JSON Response"""
        response = {}
        data = json.loads(request.body)
        try:
            link = Link.objects.create(**data)
            link.save()
            response['result'] = 'success'
            response['short_url'] = link.short_url
        except VanityException:
            response['result'] = 'fail'
            response['reason'] = "Vainity Link already taken"
        except Exception:
            response['result'] = 'fail'

        return JsonResponse(data=response)


class RedirectView(View):

    def get(self, request, *args, **kwargs):
        """Look up the short path and redirect to the real url"""

        path = request.path[:-1]

        try:
            link = Link.objects.get(short_path=path)
        except Link.DoesNotExist:
            raise Http404("Link does not exist")
        LinkView.objects.create(link=link)
        return redirect(link.redirect_link)


class StatsView(View):

    def post(self, request, *args, **kwargs):
        """Get create date, total views and views per day for the last 31 days"""

        target_url = json.loads(request.body)['path']
        link = Link.objects.get(short_path=urlparse(target_url)[2][:-1])
        views = LinkView.objects.filter(link=link)
        dates = _last_thirty_days()
        histogram = []
        for day in dates:
            histogram.append({'day': day.strftime("%b %d"), 'count': views.filter(date=day).count()})

        results = {
            'total_views': views.count(),
            'histogram': histogram,
            'created': link.date.strftime("%b %d %Y")
        }
        return JsonResponse(data=results)


def _last_thirty_days():
    """Get a list of the last 30 days"""

    sdate = datetime.today()  # start date
    edate = sdate - timedelta(30)  # end date

    delta = sdate - edate  # as timedelta
    dates = []
    for i in range(delta.days + 1):
        day = sdate - timedelta(days=i)
        dates.append(day)
    return dates