from urllib.parse import urljoin

from django.conf import settings
from django.db import models
from django.db.models import CASCADE

class VanityException(Exception):
    pass

class LinkManager(models.Manager):
    # Remove the vowels so we don't end up with unintended offesive words
    # Remove the confusing symbols like 1,l,O and 0
    _alphabet = '23456789bcdfghjkmnpqrstvwxyzBCDFGHJKLMNPQRSTVWXYZ-_'
    _base = len(_alphabet)

    def create(self, *args, **kwargs):

        if 'vanity' in kwargs:
            short_path = '/{}'.format(kwargs['vanity'])
            # Does a vanity link by that name already exist?
            try:
                self.model.objects.get(short_path=short_path)
                raise VanityException()
            except Link.DoesNotExist:
                pass

        if 'vanity' not in kwargs:
            # Does the link already exist?
            try:
                return self.model.objects.get(redirect_link=kwargs['redirect_link'])
            except Link.DoesNotExist:
                short_path = None
        # No link exists so we make one.
        create_args = {"redirect_link": kwargs['redirect_link']}
        link = super(LinkManager, self).create(*args, **create_args)
        if short_path is not None:
            link.short_path = short_path
        else:
            link.short_path = '/{}'.format(self.encode(link.id))
        link.save()
        return link

    def encode(self, number):
        """Create our short path for the url"""
        string = ''
        while (number > 0):
            string = self._alphabet[number % self._base] + string
            number //= self._base
        return string

    def decode(self, string):
        number = 0
        for char in string:
            number = number * self._base + self._alphabet.index(char)
        return number


class Link(models.Model):
    date = models.DateField(auto_now_add=True)
    redirect_link = models.CharField(max_length=2048)
    short_path = models.CharField(max_length=2048, blank=True, unique=True)

    objects = LinkManager()

    @property
    def short_url(self):
        """Return the shortened URL based on the DB ID of the record"""
        return urljoin(settings.DOMAIN, self.short_path)


class LinkView(models.Model):
    link = models.ForeignKey(Link, on_delete=CASCADE)
    date = models.DateField(auto_now_add=True)
